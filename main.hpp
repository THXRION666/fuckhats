#pragma once
#include "RakHook/rakhook.hpp"
#include "RakHook/hook_shared.hpp"

#define RPC_ScrSetPlayerAttachedObject 113

#define SPINE 1
#define HEAD 2
#define LEFT_UPPER_ARM 3
#define RIGHT_UPPER_ARM 4
#define RIGHT_HAND 6
#define LEFT_THIGH 7
#define RIGHT_THIGH 8
#define LEFT_FOOT 9
#define RIGHT_FOOT 10
#define RIGHT_CALF 11
#define LEFT_CALF 12
#define LEFT_FOREARM 13
#define RIGHT_FOREARM 14
#define LEFT_SHOULDER 15
#define RIGHT_SHOULDER 16
#define NECK 17
#define JAW 18

using RakNet::BitStream;